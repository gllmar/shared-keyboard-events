# shared-keyboard-events

capture keyboard event on a computer and send these keyboard event to another computer usign netcat

tested on macos

## dependencies

* python3
	* `brew install python` 
* python3 keyboard
	* `pip3 install keyboard`
* netcat
	

## usage 

### sender

sends the keyboards events (master)

design to be launch from a ssh command 


### receiver

receive the keyboard events (slave)

design to be launch localy

