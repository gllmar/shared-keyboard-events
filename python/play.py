import datetime
import keyboard
import fileinput
import json
import sys

#parse_event_json = lambda line: keyboard.KeyboardEvent(**json.loads(line))
#keyboard.play(parse_event_json(line) for line in fileinput.input())

data = []
#for line in fileinput.input():
#	data.append(json.loads(line))
#print(data)

press_state=-1
release_state=-1

print(datetime.datetime.now(), "INITIALIZED")

for line in fileinput.input():
	parse_event_json = keyboard.KeyboardEvent(**json.loads(line))
	keystring=str(parse_event_json)
	keystring=keystring[:-1:]
	keystring=keystring[14: :]
	key_first, *key_middle, state = keystring.split()
	if state=="down":
		press_state=1
		release_state=0
	else:
		press_state=0
		release_state=1
	key=key_first
	try:
		keyboard.send(key, press_state, release_state)
	except:
		print("unsuported caracter")
	print(datetime.datetime.now(), key, press_state, release_state)

