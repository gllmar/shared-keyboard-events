#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "running $0"
RECEIVER_IP=$(cat $DIR/config/receiver_ip)
echo "receiver_ip = $RECEIVER_IP"
PORT=$(cat $DIR/config/port)
echo "port = $PORT"

python3 -m keyboard | nc -u  $RECEIVER_IP $PORT
