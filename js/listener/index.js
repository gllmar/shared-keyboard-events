let robot = require("robotjs");
let { Server } = require('node-osc');

let oscServer = new Server(50320, '0.0.0.0');

oscServer.on('message', function (msg) 
{ 
  if (msg[0]=="/key")
  {
    let state; 
    if (msg[2]=="1"){state="down";} else{state="up";} 
    robot.keyToggle(msg[1], state);
  }
});


