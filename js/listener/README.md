
## listener in JS 

### Description

OSC input -> keyboard out

### Dependencies 

https://github.com/MylesBorins/node-osc
https://robotjs.io/docs/

### install
```
npm install node-osc robotjs
```
